import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import f1 from './Footer.module.scss';
// import { decrement, increment } from './counterSlice'

const Counter = () => {
  // const count = useSelector((state) => state.counter.value)
  // const dispatch = useDispatch()
  return (
    <footer>
      <div className={f1.shan}>
        <div className={f1.van}>PHONE</div>
        <div className={f1.van}>+43 ( 987 ) 345  - 6782</div>
      </div>
      <div className={f1.shan}>
        <div className={f1.der}>ADDRESS</div>
        <div className={f1.der}>Cracker Inc.</div>
        <div className={f1.der}>10 Cloverfield Lane</div>
        <div className={f1.der}>Berlin, IL 10928</div>
        <div className={f1.der}>Germany</div>
      </div>
      <div className={f1.shan}>SHARE US</div>
    </footer>
  )
};

export default Counter;
